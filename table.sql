CREATE TABLE metrics (
  time timestamp not null,
  metric varchar not null,
  value int not null,
  primary key (time, metric)
);

CREATE INDEX ON metrics (metric);
