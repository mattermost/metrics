import logging
import os
import sys
from datetime import datetime, timedelta
from functools import partial

import psycopg
from apscheduler.schedulers.blocking import BlockingScheduler


def connect_mattermost_db():
    try:
        mm_dsn = os.environ['MATTERMOST_DSN']
    except KeyError:
        try:
            dbhost = os.environ['POSTGRES_SERVICE_HOST']
            dbport = os.environ['POSTGRES_SERVICE_PORT']
            dbuser = os.environ['PGUSER']
            dbname = os.environ['POSTGRES_DATABASE_NAME']
            dbpass = os.environ['PGPASS']
            mm_dsn = f'user={dbuser} dbname={dbname} host={dbhost} port={dbport} password={dbpass}'
        except KeyError:
            print('No mattermost dsn nor individual credentials provided')
            sys.exit(1)

    return psycopg.connect(mm_dsn)


def connect_metrics_db():
    return psycopg.connect(os.environ['METRICS_DSN'])


def query(conn: psycopg.Connection, q, params=(), *, fetch=True, scalar=False):
    with conn.cursor() as cur:
        cur.execute(q, params)
        if fetch:
            if scalar:
                return cur.fetchone()[0]
            return cur.fetchall()


def _get_num_users(conn):
    return query(
        conn,
        """
        SELECT COUNT(*)
        FROM users u
        WHERE
            u.deleteat = 0 AND
            NOT EXISTS (SELECT 1 FROM bots b WHERE b.userid = u.id)
        """,
        scalar=True,
    )


def _get_active_users(conn, delta):
    return query(
        conn,
        """
        SELECT COUNT(*)
        FROM users u
        JOIN status st ON (st.userid = u.id)
        WHERE
            u.deleteat = 0 AND
            now() - to_timestamp(st.lastactivityat/1000) <= %s AND
            NOT EXISTS (SELECT 1 FROM bots b WHERE b.userid = u.id)
        """,
        (delta,),
        scalar=True,
    )


_get_num_users_day = partial(_get_active_users, delta=timedelta(days=1))
_get_num_users_week = partial(_get_active_users, delta=timedelta(weeks=1))
_get_num_users_month = partial(_get_active_users, delta=timedelta(days=31))


def _get_num_teams(conn):
    return query(conn, 'SELECT COUNT(*) FROM teams WHERE deleteat = 0', scalar=True)


def _get_num_channels(conn, type, delta=None):
    if delta:
        return query(
            conn,
            """
            SELECT COUNT(*)
            FROM channels
            WHERE
                deleteat = 0 AND
                type = %s AND
                now() - to_timestamp(lastpostat/1000) < %s
            """,
            (type, delta),
            scalar=True,
        )
    else:
        return query(conn, 'SELECT COUNT(*) FROM channels WHERE deleteat = 0 AND type = %s', (type,), scalar=True)


_get_num_public_channels = partial(_get_num_channels, type='O')
_get_num_private_channels = partial(_get_num_channels, type='P')
_get_num_direct_channels = partial(_get_num_channels, type='D')
_get_num_group_channels = partial(_get_num_channels, type='G')

_get_num_public_channels_day = partial(_get_num_channels, type='O', delta=timedelta(days=1))
_get_num_private_channels_day = partial(_get_num_channels, type='P', delta=timedelta(days=1))
_get_num_direct_channels_day = partial(_get_num_channels, type='D', delta=timedelta(days=1))
_get_num_group_channels_day = partial(_get_num_channels, type='G', delta=timedelta(days=1))

_get_num_public_channels_week = partial(_get_num_channels, type='O', delta=timedelta(weeks=1))
_get_num_private_channels_week = partial(_get_num_channels, type='P', delta=timedelta(weeks=1))
_get_num_direct_channels_week = partial(_get_num_channels, type='D', delta=timedelta(weeks=1))
_get_num_group_channels_week = partial(_get_num_channels, type='G', delta=timedelta(weeks=1))


def _get_num_posts(conn, delta):
    pg_now = query(conn, 'SELECT extract(epoch from now())::int', scalar=True)
    createat = (pg_now - int(delta.total_seconds())) * 1000
    return query(conn, 'SELECT COUNT(*) FROM posts WHERE createat > %s AND deleteat = 0', (createat,), scalar=True)


_get_num_posts_day = partial(_get_num_posts, delta=timedelta(days=1))
_get_num_posts_week = partial(_get_num_posts, delta=timedelta(weeks=1))


def run():
    with connect_mattermost_db() as mm_conn, connect_metrics_db() as metrics_conn:
        metrics = {
            # users
            'mattermost_num_users': _get_num_users(mm_conn),
            'mattermost_num_users_day': _get_num_users_day(mm_conn),
            'mattermost_num_users_week': _get_num_users_week(mm_conn),
            'mattermost_num_users_month': _get_num_users_month(mm_conn),
            # teams
            'mattermost_num_teams': _get_num_teams(mm_conn),
            # channels
            'mattermost_num_public_channels': _get_num_public_channels(mm_conn),
            'mattermost_num_private_channels': _get_num_private_channels(mm_conn),
            'mattermost_num_direct_channels': _get_num_direct_channels(mm_conn),
            'mattermost_num_group_channels': _get_num_group_channels(mm_conn),
            # active channels per day
            'mattermost_num_public_channels_day': _get_num_public_channels_day(mm_conn),
            'mattermost_num_private_channels_day': _get_num_private_channels_day(mm_conn),
            'mattermost_num_direct_channels_day': _get_num_direct_channels_day(mm_conn),
            'mattermost_num_group_channels_day': _get_num_group_channels_day(mm_conn),
            # active channels per week
            'mattermost_num_public_channels_week': _get_num_public_channels_week(mm_conn),
            'mattermost_num_private_channels_week': _get_num_private_channels_week(mm_conn),
            'mattermost_num_direct_channels_week': _get_num_direct_channels_week(mm_conn),
            'mattermost_num_group_channels_week': _get_num_group_channels_week(mm_conn),
            # posts per day/week (total is too slow)
            'mattermost_posts_day': _get_num_posts_day(mm_conn),
            'mattermost_posts_week': _get_num_posts_week(mm_conn),
        }
        now = datetime.now().replace(second=0, microsecond=0)
        with metrics_conn.cursor() as cur:
            cur.executemany(
                'INSERT INTO metrics (time, metric, value) VALUES (%s, %s, %s)',
                [(now, metric, val) for metric, val in metrics.items()],
            )
        metrics_conn.commit()


def main():
    logging.basicConfig()
    logging.getLogger('apscheduler').setLevel(logging.DEBUG)
    scheduler = BlockingScheduler()
    scheduler.add_job(run, 'cron', hour='8,20', minute='15')
    scheduler.start()


if __name__ == '__main__':
    main()
