FROM python:3.12-slim

# create an unprivileged user to run as
RUN set -ex && \
	groupadd -r appuser && \
	useradd -r -g appuser -m -d /app appuser

WORKDIR /app

COPY --from=ghcr.io/astral-sh/uv:latest /uv /usr/local/bin/uv
COPY uv.lock /app/uv.lock
COPY pyproject.toml /app/pyproject.toml
RUN uv sync --frozen --no-install-project

COPY app.py ./

RUN uv sync --frozen

USER appuser
CMD ["uv", "run", "--no-cache", "app.py"]
